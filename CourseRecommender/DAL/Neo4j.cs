﻿using System;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using CourseRecommender.Models;
using Neo4jClient;

namespace CourseRecommender.DAL
{
    public class Neo4j : IGraphDatabase
    {
        private IGraphClient _graphClient;
        private string _url;
        private string _user;
        private string _password;
        private Dictionary<Course, CourseInterestPool> _courseInterestPools;

        public Neo4j()
        {
            _url = ConfigurationManager.AppSettings["GraphURL"];
            _user = ConfigurationManager.AppSettings["DBUser"];
            _password = ConfigurationManager.AppSettings["DBPassword"];
            _courseInterestPools = new Dictionary<Course, CourseInterestPool>();

            ConnectToDatabase();
        }

        public void ConnectToDatabase()
        {
            if (_graphClient == null || !_graphClient.IsConnected)
            {
                try
                {
                    _graphClient = new GraphClient(new Uri(_url), _user, _password);
                    _graphClient.Connect();
                }
                catch (HttpRequestException exception)
                {
                    System.Diagnostics.Debug.WriteLine("Neo4j GraphClient exception thrown:");
                    System.Diagnostics.Debug.WriteLine(exception);
                }
            }
        }

        public bool DatabaseConnectionCheck()
        {
            if (_graphClient.IsConnected)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<Interest> GetAllInterests()
        {
            var data = _graphClient.Cypher
               .Match("(interest:Interest)")
               .Return(interest => interest.As<Interest>())
               .Results;

            return data;
        }

        public IEnumerable<Course> GetAllCourses()
        {
            var data = _graphClient.Cypher
               .Match("(course:Course)")
               .Return(course => course.As<Course>())
               .Results;

            var coursesToReturn = AssignImages((List<Course>)data);

            return coursesToReturn;
        }

        public IEnumerable<Course> GetAllCoursesWithinASubject(string subject)
        {
            var data = _graphClient.Cypher
               .Match("(subjects:Subject {area:{subject}})<-[]-(course:Course)")
               .WithParam("subject", subject)
               .Return(course => course.As<Course>())
               .Results;

            var coursesToReturn = AssignImages((List<Course>)data);

            return coursesToReturn;
        }

        public Subject GetCourseSubjectArea(Course course)
        {
            var data = _graphClient.Cypher
               .Match("(course:Course { title : {courseTitle} })-[:STUDY_AREA]->(subject:Subject)")
               .WithParam("courseTitle", course.title)
               .Return(subject => subject.As<Subject>())
               .Results
               .Single();

            return data;
        }


        public IEnumerable<Subject> GetAllSubjectAreas()
        {
            var data = _graphClient.Cypher
               .Match("(subject:Subject)")
               .Return(interest => interest.As<Subject>())
               .Results;

            return data;
        }

        public List<Course> GetCourseRecommendation(string interest)
        {
            Stopwatch sw = Stopwatch.StartNew();    // used to evaluate the query time           

            var data = _graphClient.Cypher
               .Match("(i:Interest {description : {interestDesc} })<-[:HAS_INTEREST]-(student:Student)", "(student)-[:ENROLLED_IN]->(course: Course)")
               .WithParam("interestDesc", interest)
               .Return((course, c) => new
               {
                   course = course.As<Course>(),
                   c = course.Count()
               })
               .OrderByDescending("c")
               .Limit(25)
               .Results;

            List<Course> recommendedCourses = new List<Course>();

            // Loop over all return objects and create list of courses with their frequency
            foreach (object dataobj in data)
            {
                Type type = dataobj.GetType();
                Course course = (Course)type.GetProperty("course").GetValue(dataobj, null);
                course.Frequency = (long)type.GetProperty("c").GetValue(dataobj, null);
                recommendedCourses.Add(course);
            }

            var coursesToReturn = AssignImages(recommendedCourses);

            // Query completion time recorded and outputted to browser console
            TimeSpan elapsed = sw.Elapsed;
            Debug.WriteLine("Recommendation query took: " + elapsed.TotalSeconds);

            return coursesToReturn;
        }

        public Student CreateStudentRecord(string studentName, Course course)
        {
            var data = _graphClient.Cypher
                            .Create("(student:Student {name : {studentName} })")
                            .WithParam("studentName", studentName)
                            .Return(student => student.As<Student>())
                            .Results
                            .Single();

            return data;
        }

        public void CreateSubjectArea(Subject subject)
        {
            _graphClient.Cypher
               .Create("(subject:Subject {area : {areaName} })")
               .WithParam("areaName", subject.area)
               .ExecuteWithoutResults();
        }

        public void CreateCourse(Course course)
        {
            _graphClient.Cypher
               .Create("(course:Course {title : {courseTitle} })")
               .WithParam("courseTitle", course.title)
               .ExecuteWithoutResults();
        }

        public void CreateInterest(string interest)
        {
            _graphClient.Cypher
               .Create("(interest:Interest {description  : {interestDescription} })")
               .WithParam("interestDescription", interest)
               .ExecuteWithoutResults();
        }

        public void CreateStudentCourseRelationship(Student student, Course course)
        {
            _graphClient.Cypher
                .Match("(s:Student)", "(c:Course)")
                .Where((Student s) => s.name == student.name)
                .AndWhere((Course c) => c.title == course.title)
                .Create("s-[:ENROLLED_IN]->c")
                .ExecuteWithoutResults();
        }

        public void CreateStudentInterestRelationship(Student student, string interest)
        {
            _graphClient.Cypher
                .Match("(s:Student)", "(i:Interest)")
                .Where((Student s) => s.name == student.name)
                .AndWhere((Interest i) => i.description == interest)
                .Create("s-[:HAS_INTEREST]->i")
                .ExecuteWithoutResults();
        }

        public void CreateCourseSubjectRelationship(Course course, Subject subject)
        {
            _graphClient.Cypher
                .Match("(c:Course)", "(s:Subject)")
                .Where((Course c) => c.title == course.title)
                .AndWhere((Subject s) => s.area == subject.area)
                .Create("c-[:STUDY_AREA]->s")
                .ExecuteWithoutResults();
        }

        public void DeleteAllStudents()
        {
            _graphClient.Cypher
                .Match("(student:Student)")
                .OptionalMatch("(student:Student)-[r]->(interest:Interest)")
                .OptionalMatch("(student:Student)-[r2]->(course:Course)")
                .Delete("r, r2, student")
                .ExecuteWithoutResults();
        }

        public void DeleteAllCourses()
        {
            _graphClient.Cypher
                .Match("(course:Course)")
                .Delete("course")
                .ExecuteWithoutResults();
        }

        public void DeleteAllInterests()
        {
            _graphClient.Cypher
                .Match("(interest:Interest)")
                .Delete("interest")
                .ExecuteWithoutResults();
        }

        public void DeleteAllSubjects()
        {
            _graphClient.Cypher
                .Match("(subject:Subject)")
                .OptionalMatch("(subject:Subject)<-[r]-()")
                .Delete("r, subject")
                .ExecuteWithoutResults();
        }

        public void ClearAllData()
        {
            _graphClient.Cypher
                .Match("(n)")
                .OptionalMatch("(n)<-[r]-()")
                .Delete("r, n")
                .ExecuteWithoutResults();
        }

        private List<Course> AssignImages(List<Course> courses)
        {
            List<Course> coursesWithImages = courses;

            foreach (Course course in coursesWithImages)
            {
                course.Image = string.Format("../Content/Images/{0}.PNG", course.title);
            }

            return coursesWithImages;
        }
    }
}