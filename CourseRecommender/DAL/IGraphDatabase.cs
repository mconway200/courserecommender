﻿using CourseRecommender.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseRecommender.DAL
{
    public interface IGraphDatabase
    {
        // Db connection methods
        void ConnectToDatabase();
        bool DatabaseConnectionCheck();

        // CRUD methods
        IEnumerable<Interest> GetAllInterests();
        IEnumerable<Course> GetAllCourses();
        IEnumerable<Course> GetAllCoursesWithinASubject(string subject);
        Subject GetCourseSubjectArea(Course course);
        IEnumerable<Subject> GetAllSubjectAreas();
        List<Course> GetCourseRecommendation(string interest);
        Student CreateStudentRecord(string studentName, Course course);
        void CreateSubjectArea(Subject subject);
        void CreateCourse(Course course);
        void CreateInterest(string interest);
        void CreateStudentCourseRelationship(Student student, Course course);
        void CreateStudentInterestRelationship(Student student, string interest);
        void CreateCourseSubjectRelationship(Course course, Subject subject);
        void DeleteAllStudents();
        void DeleteAllCourses();
        void DeleteAllInterests();
        void DeleteAllSubjects();
        void ClearAllData();
    }
}
