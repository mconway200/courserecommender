﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using CourseRecommender.DAL;

namespace CourseRecommender.IoC
{
    public static class IoCRegister
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            //Mvc application set up for autofac
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);
            builder.RegisterModelBinders(typeof(WebApiApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();

            // Register Neo4jClass
            builder.RegisterType<Neo4j>().As<IGraphDatabase>().InstancePerLifetimeScope();

            builder.RegisterModelBinderProvider();

            // Set the MVC dependency resolver to use Autofac
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}