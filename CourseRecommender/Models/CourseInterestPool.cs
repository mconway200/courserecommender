﻿namespace CourseRecommender.Models
{
    public class CourseInterestPool
    {
        public string[] MustHaveInterest { get; set; }
        public string[] OptinalInterests { get; set; }
    }
}