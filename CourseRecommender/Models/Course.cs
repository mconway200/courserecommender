﻿namespace CourseRecommender.Models
{
    public class Course
    {
        public string title { get; set; }

        public long Frequency { get; set; }

        public string Image { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Course)) return false;

            return ((Course)obj).title == this.title;
        }

        public override int GetHashCode()
        {
            return (this.title).GetHashCode();
        }

        public string SubjectArea { get; set; }
    }

    public enum Subjects { Accountancy, Business, Computing, DigitalMedia, Engineering, Fashion, Networking, Science };

}