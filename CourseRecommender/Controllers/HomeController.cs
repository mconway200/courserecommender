﻿using CourseRecommender.DAL;
using CourseRecommender.Models;
using CourseRecommender.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CourseRecommender.Controllers
{
    public class HomeController : Controller
    {
        private IGraphDatabase _graphDb;

        public HomeController(IGraphDatabase graphDb)
        {
            _graphDb = graphDb;
        }

        public ActionResult Index()
        {
            if (!_graphDb.DatabaseConnectionCheck())
            {
                _graphDb.ConnectToDatabase(); // retrys connection
                return View("_ConnectionError");
            }

            ViewBag.Interests = GetAllAvailableInterests();
            return View();
        }

        [HttpPost]
        public ActionResult Index(InterestViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Interests = GetAllAvailableInterests();
                return PartialView("Index", viewModel);
            }

            InterestViewModel interestViewModel = viewModel;

            try
            {
                // Creates lists of recommeneded courses to display based on selected interests
                List<Course> allRecommendedCourses = GetAndSortCourses(interestViewModel.SelectedItems.ToArray());
                List<Course> topCourseSubjectGroup = GetTopCourseSubjectGroup(allRecommendedCourses.First());
                var topRecommendedCourses = allRecommendedCourses.Take(4);  // takes the top 4 courses to display

                // Create and populate view model
                RecommendedViewModel recommendedViewModel = new RecommendedViewModel();
                recommendedViewModel.TopSubjectGroup = _graphDb.GetCourseSubjectArea(allRecommendedCourses.First()).area;
                recommendedViewModel.RecommendedCourses = topRecommendedCourses.ToList();
                recommendedViewModel.CoursesWithinSubjectGroup = topCourseSubjectGroup;
                recommendedViewModel.ChosenInterests = CreateInterestDisplayString(interestViewModel.SelectedItems.ToArray());

                return PartialView("_DisplayCourses", recommendedViewModel);
            }
            catch
            {
                _graphDb.ConnectToDatabase();   // retrys connection
                return PartialView("_ConnectionError");
            }
        }

        #region Private Implementaion

        private MultiSelectList GetAllAvailableInterests()
        {
            List<string> ListofAllInterestDescriptions = new List<string>();

            var allInterestDescriptions = _graphDb.GetAllInterests();

            foreach (Interest interest in allInterestDescriptions)
            {
                ListofAllInterestDescriptions.Add(interest.description);
            }

            return new MultiSelectList(ListofAllInterestDescriptions);
        }

        private List<Course> GetAndSortCourses(string[] interests)
        {
            List<Course> coursesToReturn = new List<Course>();

            foreach (string interest in interests)
            {
                var returnedRecommendedCourses = _graphDb.GetCourseRecommendation(interest);

                foreach (Course course in returnedRecommendedCourses)
                {
                    if (coursesToReturn.Exists(x => x.title == course.title))
                    {
                        coursesToReturn.Where(x => x.title == course.title).First().Frequency =
                            coursesToReturn.Find(x => x.title == course.title).Frequency + course.Frequency;
                    }
                    else
                    {
                        coursesToReturn.Add(course);
                    }
                }
            }

            // returns sorted list of courses based on frequency they appeared
            return coursesToReturn.OrderByDescending(o => o.Frequency).ToList();
        }

        private List<Course> GetTopCourseSubjectGroup(Course topCourse)
        {
            string subjectArea = _graphDb.GetCourseSubjectArea(topCourse).area;
            List<Course> coursesToReturn = (List<Course>)_graphDb.GetAllCoursesWithinASubject(subjectArea);

            return coursesToReturn;
        }

        private string CreateInterestDisplayString(string[] allInterests)
        {
            string interestString = "";

            foreach (string interest in allInterests)
            {
                if(allInterests.First() == interest)
                {
                    interestString = interest;                    
                }
                else
                {
                    interestString = interest + ", " + interestString;
                }
            }

            return interestString;
        }

        #endregion
    }
}
