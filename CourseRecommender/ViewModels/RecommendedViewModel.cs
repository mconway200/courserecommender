﻿using CourseRecommender.Models;
using System.Collections.Generic;

namespace CourseRecommender.ViewModels
{
    public class RecommendedViewModel
    {
        public List<Course> RecommendedCourses { get; set; }
        public List<Course> CoursesWithinSubjectGroup { get; set; }
        public string TopSubjectGroup { get; set; }
        public string ChosenInterests { get; set; }
    }
}