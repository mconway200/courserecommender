﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CourseRecommender.ViewModels
{
    public class InterestViewModel
    {
        [Required(ErrorMessage = "You must select at least one interest")]
        public IEnumerable<string> SelectedItems { get; set; }
    }
}