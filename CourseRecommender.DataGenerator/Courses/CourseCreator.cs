﻿using System.Collections.Generic;
using CourseRecommender.DAL;
using CourseRecommender.Models;

namespace CourseRecommender.DataGenerator.Courses
{
    class CourseCreator
    {
        private List<Course> _courseList;
        private List<Subject> _subjectList;
        private IGraphDatabase _graphDb;

        public CourseCreator(IGraphDatabase graphDatabase)
        {
            _graphDb = graphDatabase;
            _courseList = CreateCourseSubjectCollection();
            _subjectList = CreateSubjectAreaCollection();
        }

        public void CreateCourses()
        {
            foreach (Course course in _courseList)
            {
                _graphDb.CreateCourse(course);

                //Creates relationship between course and Subject
                _graphDb.CreateCourseSubjectRelationship(course, new Subject { area = course.SubjectArea });
            }
        }

        public void CreateSubjectAreas()
        {
            foreach(Subject subject in _subjectList)
            {
                _graphDb.CreateSubjectArea(subject);
            }
        }

        private List<Subject> CreateSubjectAreaCollection()
        {
            List<Subject> subjectAreasToReturn = new List<Subject>();

            subjectAreasToReturn.Add( new Subject { area = "Accountancy" });
            subjectAreasToReturn.Add(new Subject { area = "Business" });
            subjectAreasToReturn.Add(new Subject { area = "Computing" });
            subjectAreasToReturn.Add(new Subject { area = "DigitalMedia" });
            subjectAreasToReturn.Add(new Subject { area = "Engineering" });
            subjectAreasToReturn.Add(new Subject { area = "Fashion" });
            subjectAreasToReturn.Add(new Subject { area = "Networking" });
            subjectAreasToReturn.Add(new Subject { area = "Science" });

            return subjectAreasToReturn;
        }

        private List<Course> CreateCourseSubjectCollection()
        {
            List<Course> courseListToReturn = new List<Course>();

            courseListToReturn.Add(new Course { title = "Accountancy", SubjectArea = "Accountancy" });
            courseListToReturn.Add(new Course { title = "Finance", SubjectArea = "Accountancy" });
            courseListToReturn.Add(new Course { title = "Risk Management", SubjectArea = "Accountancy" });

            courseListToReturn.Add(new Course { title = "Marketing", SubjectArea = "Business" });
            courseListToReturn.Add(new Course { title = "International Business", SubjectArea = "Business" });

            courseListToReturn.Add(new Course { title = "Software Development", SubjectArea = "Computing" });
            courseListToReturn.Add(new Course { title = "Video Games Development", SubjectArea = "Computing" });
            courseListToReturn.Add(new Course { title = "Software Development for Business", SubjectArea = "Computing" });

            courseListToReturn.Add(new Course { title = "Computer Animation", SubjectArea = "Digital Media" });
            courseListToReturn.Add(new Course { title = "Graphic Design", SubjectArea = "Digital Media" });

            courseListToReturn.Add(new Course { title = "Computer Aided Engineering", SubjectArea = "Engineering" });
            courseListToReturn.Add(new Course { title = "Mechanical Electrical Engineering", SubjectArea = "Engineering" });

            courseListToReturn.Add(new Course { title = "Fashion Business", SubjectArea = "Fashion" });
            courseListToReturn.Add(new Course { title = "Fashion Design", SubjectArea = "Fashion" });

            courseListToReturn.Add(new Course { title = "Cyber Security", SubjectArea = "Networking" });
            courseListToReturn.Add(new Course { title = "Network Systems", SubjectArea = "Networking" });

            courseListToReturn.Add(new Course { title = "Biology", SubjectArea = "Science" });
            courseListToReturn.Add(new Course { title = "Nursing", SubjectArea = "Science" });
            courseListToReturn.Add(new Course { title = "Social Work", SubjectArea = "Science" });

            return courseListToReturn;
        }
    }
}

