﻿using System.Collections.Generic;
using CourseRecommender.DAL;

namespace CourseRecommender.DataGenerator.Interests
{
    class InterestCreator
    {
        private List<string> _interestList;
        private IGraphDatabase _graphDb;

        public InterestCreator(IGraphDatabase graphDb)
        {
            _graphDb = graphDb;
            _interestList = CreateInterestList();
        }

        public void CreateInterests()
        {
            foreach(string interest in _interestList)
            {
                _graphDb.CreateInterest(interest);
            }
        }

        private List<string> CreateInterestList()
        {
            List<string> listToReturn = new List<string>();

            listToReturn.Add("Maths");
            listToReturn.Add("Video Games");
            listToReturn.Add("Business");
            listToReturn.Add("Fashion");
            listToReturn.Add("Sports");
            listToReturn.Add("Writing");
            listToReturn.Add("Reading");
            listToReturn.Add("Design");
            listToReturn.Add("Drawing");
            listToReturn.Add("Computer Programming");
            listToReturn.Add("Graphic Design");
            listToReturn.Add("Website Design");
            listToReturn.Add("Interior Design");
            listToReturn.Add("Painting");
            listToReturn.Add("Blogging");
            listToReturn.Add("Building Computers");
            listToReturn.Add("Movies");
            listToReturn.Add("Fitness");
            listToReturn.Add("Helping Others");
            listToReturn.Add("Sculpting");
            listToReturn.Add("Martial Arts");
            listToReturn.Add("Card Games");
            listToReturn.Add("Travel");
            listToReturn.Add("Chemistry");
            listToReturn.Add("Biology");
            listToReturn.Add("Medicine");
            listToReturn.Add("Other Languages");
            listToReturn.Add("Music");
            listToReturn.Add("Finance");
            listToReturn.Add("Problem Solving");
            listToReturn.Add("Computing");
            listToReturn.Add("Engineering");
            listToReturn.Add("Internet");
            listToReturn.Add("Networking (Computing)");
            listToReturn.Add("Phycology");
            listToReturn.Add("Economics");
            listToReturn.Add("Law");
            listToReturn.Add("International Marketing");
            listToReturn.Add("Marketing");

            return listToReturn;
        }
    }
}
