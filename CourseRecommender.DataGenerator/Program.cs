﻿using CourseRecommender.DAL;

namespace CourseRecommender.DataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create new db object
            IGraphDatabase graphDb = new Neo4j();

            var generator = new Generator(graphDb);
            generator.GeneratorStart();

        }
    }
}
