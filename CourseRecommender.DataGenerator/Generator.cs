﻿using System;
using CourseRecommender.DAL;
using CourseRecommender.DataGenerator.Courses;
using CourseRecommender.DataGenerator.Interests;
using CourseRecommender.DataGenerator.Students;
using System.Collections.Generic;
using System.Linq;

namespace CourseRecommender.DataGenerator
{
    public class Generator
    {
        private IGraphDatabase _graphDb;

        public Generator(IGraphDatabase graphDb)
        {
            _graphDb = graphDb;
        }

        public void GeneratorStart()
        {
            if (!_graphDb.DatabaseConnectionCheck())
            {
                Console.WriteLine("Server Error; cannot connect to database");
                Console.ReadLine();
                Environment.Exit(0);    // exit application
            }

            string selection = DisplayMenuOptions();

            while (selection != "8")
            {
                switch (selection)
                {
                    case "1":
                        if (CourseInterestIsNullOrEmpty())
                        {
                            CreateStudents();                          
                            Console.ReadLine();
                        }
                        else
                        {
                            Console.WriteLine("You must create interests and courses before creating students");
                            Console.ReadLine();
                        }
                        break;

                    case "2":
                        CreateCourses();
                        Console.WriteLine("Creation Complete");
                        Console.ReadLine();
                        break;

                    case "3":
                        CreateInterests();
                        Console.WriteLine("Creation Complete");
                        Console.ReadLine();
                        break;

                    case "4":
                        _graphDb.DeleteAllStudents();
                        Console.WriteLine("Deletion Complete");
                        Console.ReadLine();
                        break;

                    case "5":
                        _graphDb.DeleteAllCourses();
                        _graphDb.DeleteAllSubjects();
                        Console.WriteLine("Deletion Complete");
                        Console.ReadLine();
                        break;

                    case "6":
                        _graphDb.DeleteAllInterests();
                        Console.WriteLine("Deletion Complete");
                        Console.ReadLine();
                        break;

                    case "7":
                        _graphDb.ClearAllData();
                        Console.WriteLine("Deletion Complete");
                        Console.ReadLine();
                        break;                    

                    default:
                        Console.WriteLine("Please enter a valid option");
                        Console.ReadLine();
                        break;

                }

                Console.Clear();
                selection = DisplayMenuOptions();
            }

        }

        private void CreateStudents()
        {
            // Clear DB data first
            _graphDb.DeleteAllStudents();

            Console.WriteLine("Enter number of students to create:");
            try
            {
                int numberOfStudents = Int32.Parse(Console.ReadLine());
                StudentCreator studentCreator = new StudentCreator(numberOfStudents, _graphDb);
                studentCreator.CreateStudents();
                Console.WriteLine("Creation Complete");
            }
            catch
            {
                Console.WriteLine("Please enter a valid option!");
            }
        }

        private void CreateInterests()
        {
            // Clear DB data first
            _graphDb.DeleteAllInterests();

            var interestCreator = new InterestCreator(_graphDb);

            interestCreator.CreateInterests();
        }

        private void CreateCourses()
        {
            // Clear DB data first
            _graphDb.DeleteAllCourses();
            _graphDb.DeleteAllSubjects();

            var courseCreator = new CourseCreator(_graphDb);

            courseCreator.CreateSubjectAreas();
            courseCreator.CreateCourses();
        }

        private string DisplayMenuOptions()
        {
            Console.WriteLine("Pick an option:\n");
            Console.WriteLine("1. Create Students\n");
            Console.WriteLine("2. Create Courses\n");
            Console.WriteLine("3. Create Interests\n");
            Console.WriteLine("4. Delete Students\n");
            Console.WriteLine("5. Delete Courses\n");
            Console.WriteLine("6. Delete Interests\n");
            Console.WriteLine("7. Delete All\n");
            Console.WriteLine("8. Exit\n");
            Console.WriteLine("Selction:");

            return Console.ReadLine();
        }

        private bool CourseInterestIsNullOrEmpty()
        {
            if (!_graphDb.GetAllInterests().Any() || !_graphDb.GetAllCourses().Any())
                return false;

            return true;
        }
    }
}
