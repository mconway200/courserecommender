﻿using System;
using System.Linq;
using System.Collections.Generic;
using CourseRecommender.DAL;
using CourseRecommender.Models;

namespace CourseRecommender.DataGenerator.Students
{
    class StudentCreator
    {
        private Dictionary<Course, CourseInterestPool> _interestPool;
        private List<Course> _allCourses;
        private int _totalStudents;
        private int _studentsPerCourse;
        private int _totalOptionalInterestsToAdd;
        private IEnumerable<Interest> _allInterests;
        private Random _rng;
        private IGraphDatabase _graphDb;

        public StudentCreator(int studentAmount, IGraphDatabase graphDb)
        {
            _graphDb = graphDb;
            _interestPool = new StudentInterestPool().InterestPool;
            _allCourses = (List<Course>)_graphDb.GetAllCourses();
            _totalStudents = studentAmount;
            _studentsPerCourse = _totalStudents / _allCourses.Count;
            _totalOptionalInterestsToAdd = 2;
            _allInterests = _graphDb.GetAllInterests();
            _rng = new Random();
        }

        public void CreateStudents()
        {
            foreach (Course course in _allCourses)
            {
                string courseName = course.title;
                CourseInterestPool currentCourseInterestPool;

                if(_interestPool.TryGetValue(course, out currentCourseInterestPool))
                {
                    // Creates a list of interests not used in currentCourseInterestPool
                    List<string> randomInterestPool = CreateRandomInterestPool(currentCourseInterestPool);

                    for (int i = 0; i < _studentsPerCourse; i++)
                    {
                        string studentName = string.Format("{0}_{1}", course.title, i.ToString());

                        // Create and return new student
                        var newStudent = _graphDb.CreateStudentRecord(studentName, course);
                        _graphDb.CreateStudentCourseRelationship(newStudent, course);

                        AssignMustHaveInterests(newStudent, currentCourseInterestPool.MustHaveInterest);
                        AssignOptionalInterests(newStudent, currentCourseInterestPool.OptinalInterests);
                        AssignRandomInterests(newStudent, randomInterestPool);
                    }
                }
            }
        }

        private void AssignMustHaveInterests(Student student, string[] interests)
        {
            foreach (string interest in interests)
            {
                _graphDb.CreateStudentInterestRelationship(student, interest);
            }
        }

        private void AssignOptionalInterests(Student student, string[] interests)
        {
            var values = Enumerable.Range(0, 4).OrderBy(x => _rng.Next()).ToArray();

            for (int i = 0; i < _totalOptionalInterestsToAdd; i++)
            {
                string interestToAdd = interests[values[i]];
                _graphDb.CreateStudentInterestRelationship(student, interestToAdd);
            }
        }

        private void AssignRandomInterests(Student student, List<string> interests)
        {
            int firstInterestIndex = _rng.Next(0, interests.Count);
            int secondInterestIndex = _rng.Next(0, interests.Count);

            // Repeat until integers are unique
            while (secondInterestIndex == firstInterestIndex)
            {
                secondInterestIndex = _rng.Next(0, interests.Count);
            }

            _graphDb.CreateStudentInterestRelationship(student, interests[firstInterestIndex]);
            _graphDb.CreateStudentInterestRelationship(student, interests[secondInterestIndex]);
        }

        private List<string> CreateRandomInterestPool(CourseInterestPool currentInterestPool)
        {
            var randomInterestPoolArray = _allInterests.Select(x => x.description).ToArray();
            List<string> randomInterestPoolList = new List<string>(randomInterestPoolArray);

            foreach(string interest in currentInterestPool.MustHaveInterest)
            {
                randomInterestPoolList.Remove(interest);
            }

            foreach(string interest in currentInterestPool.OptinalInterests)
            {
                randomInterestPoolList.Remove(interest);
            }

            return randomInterestPoolList;
        }
    }
}
