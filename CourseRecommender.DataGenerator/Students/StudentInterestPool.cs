﻿using System.Collections.Generic;
using CourseRecommender.DAL;
using CourseRecommender.Models;

namespace CourseRecommender.DataGenerator.Students
{
    public class StudentInterestPool
    {
        public Dictionary<Course, CourseInterestPool> InterestPool { get; set; }

        private List<Course> _allCourse;
        private string[] _mustHaveInterests;
        private string[] _optionalInterests;
        private IGraphDatabase _graphDb;

        public StudentInterestPool()
        {
            _graphDb = new Neo4j();
            _allCourse = (List<Course>)_graphDb.GetAllCourses();
            InterestPool = new Dictionary<Course, CourseInterestPool>();
            GenerateInterestPool();
        }

        private Dictionary<Course, CourseInterestPool> GenerateInterestPool()
        {
            foreach (Course course in _allCourse)
            {
                switch (course.title)
                {
                    case "Accountancy":
                        _mustHaveInterests = new string[] { "Maths", "Finance" };
                        _optionalInterests = new string[] { "Computing", "Writing", "Problem Solving", "Business" };
                        break;

                    case "Finance":
                        _mustHaveInterests = new string[] { "Business", "Maths", "Finance" };
                        _optionalInterests = new string[] { "Writing", "Computing", "Economics", "Law" };
                        break;

                    case "Risk Management":
                        _mustHaveInterests = new string[] { "Problem Solving", "Business" };
                        _optionalInterests = new string[] { "Law", "International Marketing", "Economics", "Maths" };
                        break;

                    case "Marketing":
                        _mustHaveInterests = new string[] { "Business", "Finance" };
                        _optionalInterests = new string[] { "Economics", "International Marketing", "Marketing", "Law" };
                        break;

                    case "International Business":
                        _mustHaveInterests = new string[] { "Business", "Other Languages", "Finance" };
                        _optionalInterests = new string[] { "Law", "Travel", "Marketing", "Economics" };
                        break;

                    case "Software Development":
                        _mustHaveInterests = new string[] { "Computing", "Computer Programming" };
                        _optionalInterests = new string[] { "Building Computers", "Networking (Computing)", "Internet", "Problem Solving" };
                        break;

                    case "Video Games Development":
                        _mustHaveInterests = new string[] { "Computing", "Video Games", "Computer Programming" };
                        _optionalInterests = new string[] { "Building Computers", "Internet", "Problem Solving", "Graphic Design" };
                        break;

                    case "Software Development for Business":
                        _mustHaveInterests = new string[] { "Computing", "Business", "Computer Programming" };
                        _optionalInterests = new string[] { "Internet", "Blogging", "Problem Solving", "Economics" };
                        break;

                    case "Computer Animation":
                        _mustHaveInterests = new string[] { "Graphic Design", "Design", "Computing" };
                        _optionalInterests = new string[] { "Drawing", "Painting", "Website Design", "Movies" };
                        break;

                    case "Graphic Design":
                        _mustHaveInterests = new string[] { "Graphic Design", "Design", "Drawing" };
                        _optionalInterests = new string[] { "Painting", "Computing", "Video Games", "Interior Design" };
                        break;

                    case "Computer Aided Engineering":
                        _mustHaveInterests = new string[] { "Computing", "Engineering", "Building Computers" };
                        _optionalInterests = new string[] { "Building Computers", "Maths", "Computer Programming", "Sculpting" };
                        break;

                    case "Mechanical Electrical Engineering":
                        _mustHaveInterests = new string[] { "Engineering", "Maths" };
                        _optionalInterests = new string[] { "Computing", "Sculpting", "Building Computers", "Problem Solving" };
                        break;

                    case "Fashion Business":
                        _mustHaveInterests = new string[] { "Fashion", "Business" };
                        _optionalInterests = new string[] { "Interior Design", "Marketing", "Economics", "Law" };
                        break;

                    case "Fashion Design":
                        _mustHaveInterests = new string[] { "Design", "Fashion" };
                        _optionalInterests = new string[] { "Interior Design", "Painting", "Drawing", "Graphic Design" };
                        break;

                    case "Cyber Security":
                        _mustHaveInterests = new string[] { "Computing", "Internet", "'Helping Others'" };
                        _optionalInterests = new string[] { "Blogging", "Law", "Computer Programming", "Networking (Computing)" };
                        break;

                    case "Network Systems":
                        _mustHaveInterests = new string[] { "Computing", "Building Computers", "Networking(Computing)" };
                        _optionalInterests = new string[] { "Computer Programming", "Maths", "Internet", "Design" };
                        break;

                    case "Biology":
                        _mustHaveInterests = new string[] { "Biology", "Medicine" };
                        _optionalInterests = new string[] { "Chemistry", "Helping Others", "Fitness", "Sports" };
                        break;

                    case "Nursing":
                        _mustHaveInterests = new string[] { "Biology", "Medicine", "Helping Others", "Chemistry" };
                        _optionalInterests = new string[] { "Chemistry", "Fitness", "Phycology", "Problem Solving" };
                        break;

                    case "Social Work":
                        _mustHaveInterests = new string[] { "Helping Others", "Phycology" };
                        _optionalInterests = new string[] { "Biology", "Fitness", "Socialising", "Travel" };
                        break;

                    default:
                        break;
                }

                InterestPool.Add(course, new CourseInterestPool() { MustHaveInterest = _mustHaveInterests, OptinalInterests = _optionalInterests });
            }

            return InterestPool;
        }
    }
}
